from tkinter import Text, Button, IntVar, Entry, BOTH, YES, Frame, Tk, Checkbutton, Label, CENTER, Radiobutton, END, Toplevel, PhotoImage
from tkinter.filedialog import askopenfilename, askdirectory
import docx2txt
import time
import os
from PIL import Image, ImageTk

class GUI(Frame):
	def __init__(self, master):
		super().__init__(master)
		master.option_add("*Font", ("Arial", 15))
		master.geometry("1200x600")
		master.title("Word Flashcards - Ashland University")
		self.word_frame = Frame(master)
		self.location_label = Label(master)
		self.location_button = Button(master, text="Choose File")
		self.wpm_variable = IntVar()
		self.wpm_entry = Entry(master, width=4, textvariable=self.wpm_variable)
		self.wpg_variable = IntVar()
		self.wpg_entry = Entry(master, width=4, textvariable=self.wpg_variable)
		self.go_button = Button(master, text="Go")
		self.pause_button = Button(master, text="Pause")
		self.continue_button = Button(master, text="Continue")
		self.word_text = Text(self.word_frame, height=15)
		self.flash_label = Label(self.word_frame, font=("Helvetica", 24))
		self.more_words = False
		self.input_type_var = IntVar()
		self.file_path = None
		self.control_variable = 1 # This variable is checked in the main loop, to see if it should show images/text.
		self.sleep_time = 0
		self.group_gen = None
		self.groups = []
		self.popup = Toplevel(master, width=900, height=900)
		self.extra_label = Label(self.popup, text=" ")
		self.extra_label.pack(fill=BOTH, expand=YES)
		self.popup.attributes('-topmost', 'true')
		self.popup.withdraw()
		self.start_time = None
		self.full_var = IntVar()
		self.fullscreen_option = Checkbutton(master, text="Fullscreen Images", variable=self.full_var)
		self.file_option = Radiobutton(master, text="File", variable=self.input_type_var, value=0)
		self.manual_option = Radiobutton(master, text="Cut & Paste", variable=self.input_type_var, value=1)
		self.picture_option = Radiobutton(master, text="Images", variable=self.input_type_var, value = 2)
		self.place_gui()
		self.add_functionality()
		self.show_words()

	def place_gui(self):
		self.location_label.grid(row=0, column=0)
		self.location_button.grid(row=0, column=1)
		Label(self.master, text="Words per Minute").grid(row=1, column=0)
		self.wpm_entry.grid(row=1, column=1)
		Label(self.master, text="Words per Group").grid(row=2, column=0)
		self.wpg_entry.grid(row=2, column=1)
		self.go_button.grid(row=3, column=0)
		self.pause_button.grid(row=3, column=1)
		self.continue_button.grid(row=4, column=1)
		self.file_option.grid(row=4, column=0)
		self.manual_option.grid(row=5, column=0)
		self.picture_option.grid(row=6, column=0)
		self.fullscreen_option.grid(row=7, column=0)
		self.word_frame.grid(row=0, column=2, rowspan=4, columnspan=6)
		# self.word_text.insert('0.0', "This is a test.")
		self.word_text.grid(row=0, column=0)
		self.flash_label.grid(row=1, column=0, sticky="ns")


	def add_functionality(self):
		self.location_button.configure(command=self.upload_file)
		self.go_button.configure(command=self.ready)
		self.pause_button.configure(command=self.pause)
		self.continue_button.configure(command=self.continue_action)


	def pause(self):
		self.control_variable = 1

	def continue_action(self):
		self.control_variable = 2
		self.ready()


	def ready(self):
		# verify the wpm and wpg
		self.flash_label.configure(text="")
		self.extra_label.configure(image="")
		wpm = self.wpm_variable.get()
		wpg = self.wpg_variable.get()
		if wpm <= 0:
			self.flash_label.configure(text="Not Ready: WPM must be > 0")
			return
		if wpg <= 0:
			self.flash_label.configure(text="Not Ready: WPG must be > 0")
			return
		# Get the groupings from the input
		if self.control_variable != 2:
			self.groups = self.get_input(self.input_type_var.get(), wpg)
			# print("updated groups, ", self.groups)
			if self.groups is None:
				self.flash_label.configure(text="Not Ready: Input is Invalid")
				return
			self.group_gen = self.group_generator()
		#Calculate the time needed for each group
		# self.sleep_time = int((60/wpm) * 1000)
		self.sleep_time = 60/wpm
		print(self.sleep_time)
		self.control_variable = 0
		self.start_time = time.time()
		# self.go(groups)

	def group_generator(self):
		# This is to get each group inside the main loop without blocking.
		for group in self.groups:
			yield group

	def show_words(self):
		while True:
			#This takes the place of gui.mainloop()
			#We manually update the GUI and do what we want during each update
			self.master.update()
			if self.control_variable == 1:
				#Don't go any further. It's paused.
				continue
			now = time.time()
			if now >= (self.start_time + self.sleep_time):
				#If enough time has passed to show another word or image.
				try:
					text_to_show = next(self.group_gen)
					#get the next thing to show
					if self.input_type_var.get() == 2:
						#It's an image, not text
						try:
							if self.full_var.get() == 1:
								self.popup.attributes("-fullscreen", True)
								# width, height = self.popup.winfo_width(), self.popup.winfo_height()
								# text_to_show = text_to_show.resize((width, height))
								# text_to_show = ImageTk.PhotoImage(text_to_show)
							else:
								self.popup.attributes("-fullscreen", False)
								size_str = str(text_to_show.width()) + "x" + str(text_to_show.height())
								self.popup.geometry(size_str)
								#Slightly resize and show the window
							#Show the TopLevel Window
							self.popup.deiconify()
							# print("Putting image", text_to_show)
							self.extra_label.configure(image=text_to_show)
						except Exception as e:
							print(e)
							print("Toplevel Exited")
							#User manually exited the window. Remake the Toplevel window and stop the loop
							self.popup = Toplevel(self.master, width=600, height=600)
							self.extra_label = Label(self.popup)
							self.extra_label.pack()
							self.popup.attributes('-topmost', 'true')
							self.popup.withdraw()
							self.control_variable = 1
					else:
						#It's text, put the text in the label
						group_str = " ".join(text_to_show)
						self.flash_label.configure(text=group_str)
					self.start_time = time.time()
				except StopIteration:
					print("End of text")
					#hide the window and pause
					self.popup.withdraw()
					self.control_variable = 1

	# def go(self, text):
	# 	for group in text:
	# 		start_time = time.time()
	# 		while True:
	# 			print("start = ", start_time)
	# 			now = time.time()
	# 			print("now = ", now)
	# 			if now >= (start_time + self.sleep_time):
	# 				break
	# 			self.master.update()
	# 		group_str = " ".join(group)
	# 		self.flash_label.configure(text=group_str)
	# 		# self.flash_label.after(self.sleep_time, self.master.update())
	# 		self.master.update()
	# 	self.control_variable = None


	def get_input(self, input_type, group_size):
		document = ""
		if input_type == 0:
			# Word Document
			try:
				document = docx2txt.process(self.file_path)
				document = document.replace("\n", " ")
				document = document.replace("\t", " ")
			except AttributeError:
				return None
			# print(document)
		elif input_type == 2:
			#Directory of images
			groups =[]
			for picture in sorted(os.listdir(self.file_path)):
				try:
					desire_width = 900
					desire_height = 900
					location = self.file_path + "/" + picture
					image = Image.open(location)
					if self.full_var.get() == 0:
						#if not fullscreen, then resize
						#Make larger images smaller, to fit the window
						while image.size[0] > desire_width or image.size[1] > desire_height:
							print("resizing", location)
							image = image.resize((int(image.size[0]/2), int(image.size[1]/2)), Image.ANTIALIAS)
						image = ImageTk.PhotoImage(image)
					else:
						print("resizing to fullscreen", location)
						self.popup.deiconify()
						self.popup.attributes("-fullscreen", True)
						self.popup.update()
						width, height = self.popup.winfo_width(), self.popup.winfo_height()
						print((width,height))
						image = image.resize((width, height))
						image = ImageTk.PhotoImage(image)
						# self.extra_label.configure(image=image)
					groups.append(image)
				except Exception as e:
					print(e)
					#File in the directory is not a picture, skip it.
					print("Skipping ", location)
			print("number of images = ", len(groups))
			# print(groups)
			return groups
			#Bypass the grouping logic for images
		else: # text on gui
			document = self.word_text.get("0.0", END)
			document = document.replace("\n", " ")
			document = document.replace("\t", " ")
		#Return an array of arrays of size wpg
		groups = []
		counter = 1
		current_group = []
		document = document.split(" ")
		for word in document:
			current_group.append(word)
			counter += 1
			if counter > group_size:
				groups.append(current_group)
				current_group = []
				counter = 1
		if len(current_group) != 0:
			groups.append(current_group)
		try:
			print(groups)
		except UnicodeError:
			pass
		return groups

	def upload_file(self):
		input_type = self.input_type_var.get()
		if self.input_type_var.get() == 0:
			location = askopenfilename(title="Choose File")
		elif self.input_type_var.get() == 2:
			location = askdirectory(title="Choose Directory")
		else:
			return
		if location == () or location == "": #User cancelled
			return
		self.file_path = location
		print(location)
		display_location = location.split("/")[-1]
		if len(display_location) > 15:
			#cut off path if too long (so gui doesn't resize too much)
			display_location = display_location[0:15] + "..."
		self.location_label.configure(text=display_location)






if __name__ == "__main__":
	root = Tk()
	gui = GUI(root)
